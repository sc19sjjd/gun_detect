precision = 0.886869
precision@.5 = 0.886869
recall = 0.783929
recall@.5 = 0.783929
mAP@.5 = 0.800538
mAP@.5:.95 = 0.385472