from os import walk
import xml.etree.ElementTree as ET

#base path
PATH = "C:/Users/Stuart/Documents/gun_detect/"

#path of dataset to be converted
dataset = PATH + "datasets/youtube/"

#get all label file names from dataset path
files = []
for (dirpath, dirnames, filenames) in walk(dataset + "labels_xml/"):
    files.extend(filenames)
    break

#iterate through all files
for fname in files:
    #parse the label file
    root = ET.parse(dataset + "labels_xml" + fname).getroot()
    
    #get image size data
    width = float(root.find('size/width').text)
    height = float(root.find('size/height').text)
    
    instances = []
    #iterate through each object (handgun)
    for obj in root.findall('object'):
        #get bounding box data
        xmin = float(obj.find('bndbox/xmin').text)
        ymin = float(obj.find('bndbox/ymin').text)
        xmax = float(obj.find('bndbox/xmax').text)
        ymax = float(obj.find('bndbox/ymax').text)
        
        #now convert to yolo format (class x_centre y_centre box_width box_height)
        xc = (xmin + xmax) / 2
        yc = (ymin + ymax) / 2
        bw = xmax - xmin
        bh = ymax - ymin
        #yolo format requires these values to be normalised (between 0 and 1)
        xc = xc / width
        yc = yc / height
        bw = bw / width
        bh = bh / height
        
        #add new handgun instance string (0 for class)
        line = "0 %s %s %s %s\n" % (xc, yc, bw, bh)
        instances.append(line)
        
    #create new file
    fpath = dataset + "labels_txt/" + fname.replace('.xml', '.txt')
    with open(fpath, 'w') as f:
        #write each handgun instance
        f.writelines(instances)