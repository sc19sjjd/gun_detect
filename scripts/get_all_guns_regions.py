import cv2
from os import walk

METHOD = 2
assert METHOD == 1 or METHOD == 2
if METHOD == 1:
    METHOD_NAME = "method1"
else:
    METHOD_NAME = "method2"
    
DATASET = "val"

label_path = "C:/Users/Stuart/Documents/gun_detect/detect/" + DATASET + "/labels/"
image_path = "C:/Users/Stuart/Documents/gun_detect/detect/" + DATASET + "/images/"

#get all label file names from dataset
files = []
for (dirpath, dirnames, filenames) in walk(label_path):
    files.extend(filenames)
    break

#step through all file names
for fname in files:
    print("Processing: %s" % (fname))
    
    #get the image
    img = cv2.imread(image_path + fname.replace('.txt', '.jpg'))
    img_dim = img.shape
    WIDTH = img_dim[1]
    HEIGHT = img_dim[0]
    
    #get the label
    with open(label_path + fname, 'r') as f:
        instances = f.readlines()
    
    count = 1
    for inst in instances:
        box = inst.split()
        #yolo format: (class xcentre ycentre width height)
        box = inst.split()
        bx = float(box[1]) * WIDTH
        by = float(box[2]) * HEIGHT
        bw = float(box[3]) * WIDTH
        bh = float(box[4]) * HEIGHT
        
        new_img_name = fname.replace('.txt', '_' + METHOD_NAME + '_' + str(count) + '.jpg')
        new_img_path = image_path.replace('/detect', '/detect_' + METHOD_NAME) + new_img_name
        new_label_name = new_img_name.replace('.jpg', '.txt')
        new_label_path = label_path.replace('/detect', '/detect_' + METHOD_NAME) + new_label_name
        
        #calculate the new hand region, centre x and y is same as bndbox
        size = bw * 2.5
        size = max(size, bh * 3)
        hand_w = size
        hand_h = size

            
        #get x and y vals and check if they extend past the image size
        xmin = max(0, bx - hand_w / 2)
        xmax = min(WIDTH, bx + hand_w / 2)
        ymin = max(0, by - hand_h / 2)
        ymax = min(HEIGHT, by + hand_h / 2)
        hand_w = xmax - xmin
        hand_h = ymax - ymin
            
        #get the new bounding box values
        x = (bx - xmin) / hand_w
        y = (by - ymin ) / hand_h
        w = bw / hand_w
        h = bh / hand_h
        new_box = "0 %f %f %f %f" % (x, y, w, h)
        
        try:
            #save the new image
            new_img = img[int(ymin):int(ymax),int(xmin):int(xmax)]
            cv2.imwrite(new_img_path, new_img)
            #show saved image and gun bounding box
            new_img = cv2.resize(new_img, (300, 300), interpolation = cv2.INTER_AREA)
            cv2.rectangle(new_img, (int((x - w/2)*300), int((y - h/2)*300)), (int((x + w/2)*300), int((y + h/2)*300)), (255,0,255), 2)
            cv2.imshow("Hand Region", new_img)
            
            #save the new label
            with open(new_label_path, 'w') as f:
                #write the handgun instance
                f.write(new_box)
        except:
            print("failed to save: " + new_img_path)
            
        #increment count
        count += 1
        
    cv2.imshow("Image", img)
    key = cv2.waitKey(15)
    if key == 27: break
        
    