import wandb
import sys
import cv2
import os
from sys import platform
import argparse
import math
import numpy as np
from os import walk
import re
import matplotlib.pyplot as plt

#the test set being used
SET = 'test2'

#the training model being tested
MODEL = "yolo_only_final"

#flag for whether or not hand region estimation is performed
#must be correct or results will be incorrect
IS_HAND_REGION = False

#method for hand region estimation (does not affect yolo only model)
METHOD = MODEL.split('_')[1]
REGION_DIR = "../detect_" + METHOD + "/" + SET + "/regions/"

#these are the dimentions of the full images when performing the hand estimation
#can be found in scripts/extract_hand_areas.py
IMG_WIDTH = 1280
IMG_HEIGHT = 962
#default image size for yolo train.py and val.py is 640
TEST_IMG_SIZE = 640
#test set must first be run and saved through val.py in yolov5
TEST_PATH = "../runs/test/" + MODEL + "_" + SET + "/labels/"

#paths for full images and labels
IMG_PATH = "../detect/" + SET + "/images/"
LABEL_PATH = "../detect/" + SET + "/labels/"

#iou threshold for NMS is 0.6 in val.py
IOU_THRESH = 0.6
    
#union of 2 rectangles a, b
#a and b in the form (xmin, ymin, width, height)
def getUnion(a, b):
    area1 = a[2] * b[3]
    area2 = b[2] * b[3]
    union = area1 + area2 - getIntersection(a, b)
    
    return union

#intersection of rectangles a, b
#a and b in the form (xmin, ymin, width, height)
def getIntersection(a, b):
    x = max(a[0], b[0])
    y = max(a[1], b[1])
    w = min(a[0] + a[2], b[0] + b[2]) - x
    h = min(a[1] + a[3], b[1] + b[3]) - y
    intersection = w * h
    
    if w < 0 or h < 0:
        return 0
    else:
        return intersection

#get the IoU of 2 rectangles a, b
#a and b in the form (xmin, ymin, width, height)
def getIoU(a, b):
    intersection = getIntersection(a, b)
    union = getUnion(a, b)
    iou = intersection / union
    
    return iou

#perform non max suppression on a list of instances of bounding boxes
def NMS(instances, threshold):
    #sort instances in order of confidence score
    in_order = []
    is_suppressed = []
    max_val = 0
    max_i = 0
    for i in range(len(instances)):
        for j in range(len(instances)):
            if float(instances[j].split()[5]) > max_val:
                max_val = float(instances[j].split()[5])
                max_i = j
                
        
        is_suppressed.append(False)
        in_order.append(instances[max_i])
        instances.pop(max_i)
        max_val = 0
    instances = in_order

    #iterate through all instances ordered by highest conf score
    for i in range(len(instances)):
        #check if the box has already been suppressed
        if is_suppressed[i]:
            continue
        
        bbox1 = list(map(float, instances[i].split()))
        #now perform non max suppression on the highest confidence and all other bndboxes
        for j in range(len(instances)):
            if i == j:
                continue
            bbox2 = list(map(float, instances[j].split()))
            #rectangles must be of form (xmin, ymin, width, height)
            a = (bbox1[1] - float(bbox1[3]/2), bbox1[2] - float(bbox1[4]/2), bbox1[3], bbox1[4]) 
            b = (bbox2[1] - float(bbox2[3]/2), bbox2[2] - float(bbox2[4]/2), bbox2[3], bbox2[4])
            iou = getIoU(a, b)
            #if iou is above threshold, then suppress the lower confidence bbox
            if iou > threshold:
                is_suppressed[j] = True
     
    new_instances = []
    #now keep all unsuppressed boxes
    for i in range(len(instances)):
        if not is_suppressed[i]:
            new_instances.append(instances[i])
    
    return new_instances
            
#rescales the hand region bounding boxes for the full image size
def rescale(instances, region_path):
    #get the xmin and ymin of the original hand region (hr)
    with open(region_path, 'r') as f:
        region = f.read()
        hr_w, hr_h, hr_xmin, hr_ymin = list(map(float, region.split()))
        
    new_instances = []
    #iterate through instances
    for instance in instances:
        bbox = list(map(float, instance.split()))
        #calculate original xcentre, ycentre, width and height for bounding box
        x = ((bbox[1] * hr_w) + hr_xmin) / IMG_WIDTH
        y = ((bbox[2] * hr_h) + hr_ymin) / IMG_HEIGHT
        w = (bbox[3] * hr_w) / IMG_WIDTH 
        h = (bbox[4] * hr_h) / IMG_HEIGHT
        original_bbox = "%f %f %f %f %f %f" % (bbox[0], x, y, w, h, bbox[5])
        #add rescaled bounding box instance
        new_instances.append(original_bbox) 
        
    return new_instances
    
#filters out box predictions with confidence scores lower than confidence threshold
def filterLowConfidence(instances, conf_thresh):
    new_instances = []
    for instance in instances:
        bbox = list(map(float, instance.split()))
        if bbox[5] > conf_thresh:
            new_instances.append(instance)
            
    return new_instances



#get all label file names from full images test set path
true_files = []
for (dirpath, dirnames, true_filenames) in walk(LABEL_PATH):
    true_files.extend(true_filenames)
    break
#get all label file names from the val test path
test_files = []
for (dirpath, dirnames, test_filenames) in walk(TEST_PATH):
    test_files.extend(test_filenames)
    break

#print(test_files)

#confidence threshold, discard predictions with confidence lower than threshold
#use a range from 0 to 1 so that a precision recall graph can be produced and used to calculate AP
num_points = 101
conf_thresholds = np.linspace(0.01, 0.99, num_points-2)
pred_thresholds = np.linspace(0.5, 0.95, 10)
#pred_thresholds = [0.5]
precision_all = []
recall_all = []
f1_all = []
AP_all = []
TP_all = 0
FP_all = 0
FN_all = 0
#print(pred_thresholds)
""" """
for pred_thresh in pred_thresholds:

    print("\nPredicion iou threshold = " + str(pred_thresh))

    #set lists    
    precision_list = [0]
    recall_list = [1]
    f1_list = [0]
    
    for conf_thresh in conf_thresholds:
        sys.stdout.write("\rtest at conf-thresh = " + "{:.2f}".format(conf_thresh))
        sys.stdout.flush()
        TP = 0 #true positives
        FP = 0 #false positives
        FN = 0 #false negatives
        #iterate through the full images and labels
        for fname in true_files:
            #sys.stdout.write("\rProcessing %s" % (fname))
            sys.stdout.flush()
            #get the full image and ground truth bounding box instances
            img = cv2.imread(IMG_PATH + fname.replace('.txt', '.jpg'))
            with open(LABEL_PATH + fname, 'r') as f:
                true_instances = f.readlines()
        
            #find all predicted labels with the same base file name
            base_name = fname.replace('.txt', '')
            r = re.compile(".*" + base_name)
            test_labels = list(filter(r.match, test_files))
    
            #if list is empty then no predictions were made on this image
            if len(test_labels) == 0:
                #print("No predictions for " + base_name)
                #increment false negatives accordingly and then continue to next fname
                FN += len(true_instances)
                continue
    
            #get all bounding box instances from the test labels
            test_instances = []
            for label_name in test_labels:
                with open(TEST_PATH + label_name) as f:
                    temp_instances = f.readlines()
                    #if hand region estimation is used then the bounding boxes must be recalculated for the full image
                    if IS_HAND_REGION:
                        temp_instances = rescale(temp_instances, REGION_DIR + label_name)
                #append all new instances
                for instance in temp_instances:
                    test_instances.append(instance)
                
            test_instances = filterLowConfidence(test_instances, conf_thresh)
        
            #perform non-max supression on all test bounding boxes (only necessary when using hand region estimation)
            if IS_HAND_REGION:
                test_instances = NMS(test_instances, IOU_THRESH)
    
            #count the number of correct predictions for this image
            correct_predicts = 0
            #go through each ground truth bounding box instance
            for true_instance in true_instances:
                found = False
                true_bbox = list(map(float, true_instance.split()))
                #check if any test instances have an IoU over the prediction threshold
                for test_instance in test_instances:
                    test_bbox = list(map(float, test_instance.split()))
                    #rectangles must be of form (xmin, ymin, width, height)
                    a = (true_bbox[1] - true_bbox[3]/2, true_bbox[2] - true_bbox[4]/2, true_bbox[3], true_bbox[4]) 
                    b = (test_bbox[1] - test_bbox[3]/2, test_bbox[2] - test_bbox[4]/2, test_bbox[3], test_bbox[4]) 
                    iou = getIoU(a, b)
                    #correct predictions are made when iou is above prediction threshold
                    if iou > pred_thresh and found == False:
                        correct_predicts += 1
                        found = True
            
            #now calculate the number of TPs, FPs and FNs, add them to counters
            TP += correct_predicts
            FP += len(test_instances) - correct_predicts
            FN += len(true_instances) - correct_predicts
            
            #print("\nTP: %f,    FP: %f     FN: %f\n" % (TP, FP, FN))
    

        #final test evaluation metrics calculations
        #divide by 0 exceptions
        try:
            precision = TP / (TP + FP)
        except:
            precision = 1
        recall = TP / (TP + FN)
        try:
            f1 = (2 * precision * recall) / (precision + recall) #f-measure
        except:
            f1 = 0
            print("\nDivsion by 0, test instances: %i, true instances: %i\n" % (len(test_instances), len(true_instances)))

        precision_list.append(precision)
        recall_list.append(recall)
        f1_list.append(f1) 
        
        TP_all += TP
        FP_all += FP
        FN_all += FN
        
        #print("\nprecision: %f\nrecall: %f\nf1: %f\n" % (precision, recall, f1))

    precision_list.append(1)
    recall_list.append(0)
    f1_list.append(0)
    
    #calculate AP and find max f1 index
    AP = 0
    max_pr = 0
    max_i = 0
    max_f1 = 0
    for i in range(num_points-1):
        AP += (recall_list[i] - recall_list[i+1]) * precision_list[i]
        if precision_list[i] + recall_list[i] > max_pr:
            max_pr = precision_list[i] + recall_list[i]
            max_i = i
        if f1_list[i] > f1_list[max_f1]:
            max_f1 = i
            
    
    print("\n\nPrecision = %f, recall = %f, AP = %f" % (precision_list[max_f1], recall_list[max_f1], AP))
    
    AP_all.append(AP)
    precision_all.append(precision_list[max_f1])
    recall_all.append(recall_list[max_f1])
    f1_all.append(f1_list[max_f1])
    
    fig, ax = plt.subplots()
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    plt.axis([0, 1, 0, 1])
    plt.title("Precision-recall curve @" + "{:.2f}".format(pred_thresh) + " prediction threshold ")
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.grid()
    plt.plot(recall_list, precision_list)
    plt.show()

    

#calculate final precision, recall and mAP
mAP95 = 0
max_f1 = 0
for i in range(10):      
    if f1_all[i] > f1_all[max_f1]:
        max_f1 = i
    
    mAP95 += AP_all[i]

precision_final = precision_all[max_f1]
recall_final = recall_all[max_f1]
mAP95 = mAP95 / 10

print("\nMax f1")
final_results = ("precision = %f\nprecision@.5 = %f\nrecall = %f\nrecall@.5 = %f\nmAP@.5 = %f\nmAP@.5:.95 = %f" %
                (precision_final, precision_all[0], recall_final, recall_all[0], AP_all[0], mAP95))
print("" + final_results)


with open("../runs/test/" + MODEL + "_" + SET + "/test_results.txt", "w") as f:
    f.write(final_results)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    