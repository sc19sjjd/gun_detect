# -*- coding: utf-8 -*-
from os import walk
import random
import shutil

PATH = "C:/Users/Stuart/Documents/gun_detect/"

#path of dataset to be allocated
DATASET = "youtube"

image_path = PATH + "datasets/" + DATASET + "/images/"
label_path = PATH + "datasets/" + DATASET + "/labels_txt/"



#allocations for dataset (must add to 1)
train = 0.8
valid = 0.2
assert train + valid == 1
#no test allocation as that is all from the mock attack dataset


def allocate(fname, dest):
    #copy the file into new directory
    #labels
    src = label_path + fname
    dst = PATH + "detect/" + dest + "/labels/" + fname
    shutil.copy(src, dst)
    #images
    src = image_path + fname.replace('.txt', '.jpg')
    dst = PATH + "detect/" + dest + "/images/" + fname.replace('.txt', '.jpg')
    shutil.copy(src, dst)


#get all label file names from dataset path
files = []
for (dirpath, dirnames, filenames) in walk(label_path):
    files.extend(filenames)
    break

train_num = int(len(files) * train)
valid_num = len(files) - train_num
#list to hold indexes already selected
selected = []
#add all validation set
print("Allocating...\n")
for i in range(valid_num):
    #get a unique random index
    num = random.randint(0, len(files)-1)
    while num in selected:
        num = random.randint(0, len(files)-1)
    #add new index to list
    selected.append(num)
    
    #copy the file into new directory
    fname = files[num]
    allocate(fname, 'val')
    
#add remaining files to train set
for i in range(len(files)):
    #continue if file already in valid set
    if i in selected:
        continue
    #else copy file into new directory
    else:
        fname = files[i]
        allocate(fname, 'train')
        
print("Allocation:")
print("   train: %s\n   valid: %s\n   total: %s" % (train_num, valid_num, len(files)))