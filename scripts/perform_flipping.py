# -*- coding: utf-8 -*-
from os import walk
import random
import cv2

#base path
PATH = "C:/Users/Stuart/Documents/gun_detect/detect/val/"

#factor of the train set to be horizontally flipped
flip = 0.7

#get all label file names from train path
files = []
for (dirpath, dirnames, filenames) in walk(PATH + "labels/"):
    files.extend(filenames)
    break

#dataset counts
gmd = 0
mgd_c = 0
mgd_v = 0
yt = 0

total = int(len(files) * flip)
#list to hold indexes already selected
selected = []
#flip specified total number of images
print("Flipping %s images...\n" % (total))
for i in range(total):
    #get a unique random index
    num = random.randint(0, len(files)-1)
    while num in selected:
        num = random.randint(0, len(files)-1)
    #add new index to list
    selected.append(num)
    
    fname = files[num]
    #record which dataset this file belongs to
    if "gmd" in fname:
        gmd += 1
    elif "mgd_custom" in fname:
        mgd_c += 1
    elif "mgd_varying" in fname:
        mgd_v += 1
    elif "youtube" in fname:
        yt += 1
    
    #flip the bounding boxes locations
    fpath = PATH + "labels/" + fname
    with open(fpath, 'r') as f:
        instances = f.readlines()
    
    new_instances = []
    for box in instances:
        #yolo format is: class x_centre y_centre box_width box_height
        data = box.split()
        #to flip horizontally we need to only change x_centre
        new_x = 1 - float(data[1])
        line = "0 %s %s %s %s" % (new_x, data[2], data[3], data[4])
        new_instances.append(line)
        
    #write new image label
    fpath = PATH + "labels/" + fname.replace('.txt', '_flip.txt')
    with open(fpath, 'w') as f:
        #write each instance
        f.writelines(new_instances)
        
    #now flip image
    fname = fname.replace('.txt', '.jpg')
    fpath = PATH + "images/" + fname
    img = cv2.imread(fpath)
    img_flip = cv2.flip(img, 1)
    #save new flipped image
    cv2.imwrite(fpath.replace('.jpg', '_flip.jpg'), img_flip)
    
print("Finished, total flipped:")
print("  gmd: %s\n  mgd_c: %s\n  mgd_v: %s\n  yt: %s" % (gmd, mgd_c, mgd_v, yt))
    

