
from os import walk
import shutil
import random

#old and new paths of images and annotations
old_path_im = "../detect_method2/train/images_empty/"
new_path_im = "../detect_method2/train/images/"
old_path_lab = "../detect_method2/train/labels_empty/"
new_path_lab = "../detect_method2/train/labels/"

#total number of images to be randomly selected (10% of images with guns)
total = 350

#get all file names
f = []
for (dirpath, dirnames, filenames) in walk(old_path_im):
    f.extend(filenames)
    break

#list to hold all indexes already selected
all_num = []
for i in range(total):
    #get a unique random number
    num = random.randint(0, len(f)-1)
    while num in all_num:
        num = random.randint(0, len(f)-1)
        
    #add new number to list
    all_num.append(num)
    
    #copy the file into new directory
    fname = f[num]
    src = old_path_im + fname
    dst = new_path_im + fname
    shutil.copy(src, dst)
    
    src = old_path_lab + fname.replace('jpg', 'txt')
    dst = new_path_lab + fname.replace('jpg', 'txt')
    shutil.copy(src, dst)
    
    print(fname + " selected at index " + str(num))