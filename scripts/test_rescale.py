import wandb
import sys
import cv2
import os
from sys import platform
import argparse
import math
import numpy as np
from os import walk
import re
import matplotlib.pyplot as plt

img_l_p = "../detect/test1/images/mgd_custom_frame355.jpg"
img_s_p = "../detect_method1/test1/images/mgd_custom_frame355_method1_1.jpg"
label_p = "../detect_method1/test1/labels/mgd_custom_frame355_method1_1.txt"
region_p = "../detect_method1/test1/regions/mgd_custom_frame355_method1_1.txt"

IMG_WIDTH = 1280
IMG_HEIGHT = 962

def rescale(region_path, instances):
    #get the xmin and ymin of the original hand region (hr)
    with open(region_path, 'r') as f:
        region = f.read()
        hr_w, hr_h, hr_xmin, hr_ymin = list(map(float, region.split()))
        #hr_w = int(hr_w)
        #hr_h = int(hr_h)
        
    new_instances = []
    #iterate through instances
    for instance in instances:
        bbox = list(map(float, instance.split()))
        #calculate original xcentre, ycentre, width and height for bounding box
        x = ((bbox[1] * hr_w) + hr_xmin) / IMG_WIDTH
        y = ((bbox[2] * hr_h) + hr_ymin) / IMG_HEIGHT
        w = (bbox[3] * hr_w) / IMG_WIDTH 
        h = (bbox[4] * hr_h) / IMG_HEIGHT
        original_bbox = "%f %f %f %f %f" % (bbox[0], x, y, w, h)
        #add rescaled bounding box instance
        new_instances.append(original_bbox) 
        
    return new_instances

img = cv2.imread(img_l_p)
img = cv2.resize(img, (640,480))
img_s = cv2.imread(img_s_p)
with open(label_p, 'r') as f:
    instances = f.readlines()


instances = rescale(region_p, instances)

for instance in instances:
    bbox = list(map(float, instance.split()))
    x = bbox[1] * 640
    y = bbox[2] * 480
    w = bbox[3] * 640
    h = bbox[4] * 480
    
    cv2.rectangle(img, (int(x-w/2), int(y-h/2)), (int(x+w/2), int(y+h/2)), (255,0,255), 2)

cv2.imshow("Hand Region Extraction", img)
key = cv2.waitKey(15)