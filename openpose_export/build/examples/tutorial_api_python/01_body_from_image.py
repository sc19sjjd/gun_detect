# From Python
# It requires OpenCV installed for Python
import sys
import cv2
import os
from sys import platform
import argparse
import math


def normaliseKeypoints(keypoints):
    neck = keypoints[1]
    neckx, necky, conf = neck
    
    hip = keypoints[8]
    hipx, hipy, conf = hip
    
    dist = math.sqrt((hipx-neckx)**2 + (hipy-necky)**2)
    
    normalised = []
    for keypoint in keypoints:
        ox = keypoint[0]
        oy = keypoint[1]
        if ox == 0 or oy == 0:
            new = [ox, oy]
        else:
            nx = (ox - neckx) / dist
            ny = (oy - necky) / dist
            new = [nx, ny]
        normalised.append(new)
        
    return normalised

try:
    # Import Openpose (Windows/Ubuntu/OSX)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    try:
        # Windows Import
        if platform == "win32":
            # Change these variables to point to the correct folder (Release/x64 etc.)
            sys.path.append(dir_path + '/../../python/openpose/Release');
            os.environ['PATH']  = os.environ['PATH'] + ';' + dir_path + '/../../x64/Release;' +  dir_path + '/../../bin;'
            import pyopenpose as op
        else:
            # Change these variables to point to the correct folder (Release/x64 etc.)
            sys.path.append('../../python');
            # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
            # sys.path.append('/usr/local/python')
            from openpose import pyopenpose as op
    except ImportError as e:
        print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
        raise e

    # Flags
    parser = argparse.ArgumentParser()
    parser.add_argument("--image_path", default="../../../../datasets/gmd/images/gmd_1_frame12.jpg", help="Process an image. Read all standard formats (jpg, png, bmp, etc.).")
    args = parser.parse_known_args()

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    params["model_folder"] = "../../../models/"

    # Add others in path?
    for i in range(0, len(args[1])):
        curr_item = args[1][i]
        if i != len(args[1])-1: next_item = args[1][i+1]
        else: next_item = "1"
        if "--" in curr_item and "--" in next_item:
            key = curr_item.replace('-','')
            if key not in params:  params[key] = "1"
        elif "--" in curr_item and "--" not in next_item:
            key = curr_item.replace('-','')
            if key not in params: params[key] = next_item

    # Construct it from system arguments
    # op.init_argv(args[1])
    # oppython = op.OpenposePython()

    # Starting OpenPose
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()

    # Process Image
    datum = op.Datum()
    imageToProcess = cv2.imread(args[0].image_path)
    datum.cvInputData = imageToProcess
    opWrapper.emplaceAndPop(op.VectorDatum([datum]))
    
    nkeypoints = normaliseKeypoints(datum.poseKeypoints[0])

    # Display Image
    print("Body keypoints: \n" + str(datum.poseKeypoints))
    print("Normalised keypoints: \n" + str(nkeypoints))
    right = datum.poseKeypoints[0][4]
    print("Right wrist keypoint: " + str(right))
    img_dim = imageToProcess.shape
    x = right[0] 
    cv2.imshow("OpenPose 1.7.0 - Tutorial Python API", datum.cvOutputData)
    cv2.waitKey(0)
    
    opWrapper = None
except Exception as e:
    print(e)
    opWrapper = None
    sys.exit(-1)
