import sys
import cv2
import os
from sys import platform
import argparse
import math
import time

WIDTH = 1280 
HEIGHT = 962

#method 1 or 2
METHOD = 1
assert METHOD == 1 or METHOD == 2

if METHOD == 1:
    METHOD_NAME = "method1"
else:
    METHOD_NAME = "method2"

DATASET = "val"

#union of 2 rectangles a, b
#a and b in the form (xmin, ymin, widht, height)
def getUnion(a, b):
    xmin = min(a[0], b[0])
    ymin = min(a[1], b[1])
    w = max(a[0] + a[2], b[0] + b[2]) - xmin
    h = max(a[1] + a[3], b[1] + b[3]) - ymin
    
    return (xmin, ymin, w, h)

#intersection of 2 rectangles a, b
#a and b in the form (xmin, ymin, width, height)
def getIntersection(a, b):
    xmax = max(a[0], b[0])
    ymax = max(a[1], b[1])
    w = min(a[0] + a[2], b[0] + b[2]) - xmax
    h = min(a[1] + a[3], b[1] + b[3]) - ymax
    
    if w<=0 or h<=0: 
        return (0,0,0,0)
    
    return (xmax, ymax, w, h)

#method 1 for hand region estimation, elbow and wrist are keypoints
def getHandRegion1(elbow, wrist, back_dist):
    #get distance between elbow and wrist
    arm_dist = math.sqrt( (wrist[0] - elbow[0])**2 + (wrist[1] - elbow[1])**2 )

    #calculate centre of hand region using parametric form of line
    t = -0.001687 * arm_dist + 1.576804
    xc = (1 - t) * elbow[0] + t * wrist[0]
    yc = (1 - t) * elbow[1] + t * wrist[1]
        
    #use back or arm length to determine the size of the region
    #size = 25 + back_dist / 1.5
    if arm_dist > back_dist:
        size = 35 + arm_dist / 1.2
    else: 
        size = 25 + back_dist / 1.2
    w = size
    h = size
    
    xmin = float(xc - size / 2)
    xmax = float(xc + size / 2)
    ymin = float(yc - size / 2)
    ymax = float(yc + size / 2)
    
    return (xc, yc, w, h, xmin, ymin, xmax, ymax)


#get the points of intersection between a line y = mx + c that goes 
#through the centre of a circle r^2 = (x - a)^2 + (y - b)^2
def circleIntersection(m, c, a, b, r):
    aprim = (1 + m**2)
    bprim = 2 * m * (c - b) - 2 * a
    cprim = a**2 + (c - b)**2 - r**2
    delta = bprim**2 - 4 * aprim * cprim
    
    x1 = (-bprim + math.sqrt(delta)) / (2 * aprim)
    y1 = m * x1 + c
    p1 = (x1, y1)
    
    x2 = (-bprim - math.sqrt(delta)) / (2 * aprim)
    y2 = m * x2 + c
    p2 = (x2, y2)
    
    return p1, p2

#returns the max value from a list
def getMax(numbers):
    m = numbers[0]
    for num in numbers:
        m = max(m, num)
        
    return m

#returns the minimum number from a list
def getMin(numbers):
    m = numbers[0]
    for num in numbers:
        m = min(m, num)
        
    return m

#method 2 for hand region estimation
def getHandRegion2(elbow, wrist, back_dist):
    #get distance between elbow and wrist
    arm_dist = math.sqrt( (wrist[0] - elbow[0])**2 + (wrist[1] - elbow[1])**2 )
    
    #calculate t for the point (p1) that extends past the gun
    if arm_dist > back_dist:
        t = 1.8 + arm_dist / 80
    else:
        t = 1.8 + back_dist / 100
    t = 2.5
    #extend the line from elbow to wrist
    x = (1 - t) * elbow[0] + t * wrist[0]
    y = (1 - t) * elbow[1] + t * wrist[1]
    p1 = (x, y)
    
    #calculate the point (p2) that is slightly behind the wrist, between the elbow
    t = 0.88
    x = (1 - t) * elbow[0] + t * wrist[0]
    y = (1 - t) * elbow[1] + t * wrist[1]
    p2 = (x, y)
    
    #now calculate the gradient of the normal for the line from p1 and p2
    dx = (p1[0] - p2[0])
    dy = (p1[1] - p2[1])
    grad = dy / dx
    grad_norm = -(1 / grad)
    
    #find points p3 and p4 that are distance r away from point p1 and lie on the normal
    #where r is the radius of a circle with centre p1 that intersects the normal line at p3 and p4
    r = 18 + arm_dist / 1.1
    #find c for the normal line y = grad_norm*x + c that goes through p1, p3 and p4
    c = p1[1] - grad_norm * p1[0]
    #get the points of intersection
    p3, p4 = circleIntersection(grad_norm, c, p1[0], p1[1], r)
    
    #find points p5 and p6 that are distance r away from point p2 and lie on the normal
    #where r is the radius of a circle with centre p2 that intersects the normal line at p4 and p5
    r = 12 + arm_dist / 1.6
    #find c for the normal line y = grad_norm*x + c that goes through p2, p5 and p6
    c = p2[1] - grad_norm * p2[0]
    #get the points of intersection
    p5, p6 = circleIntersection(grad_norm, c, p2[0], p2[1], r)
    
    #now get the hand region values
    xmin = getMin([p1[0], p2[0], p3[0], p4[0], p5[0], p6[0]])
    ymin = getMin([p1[1], p2[1], p3[1], p4[1], p5[1], p6[1]])
    xmax = getMax([p1[0], p2[0], p3[0], p4[0], p5[0], p6[0]])
    ymax = getMax([p1[1], p2[1], p3[1], p4[1], p5[1], p6[1]])
    w = xmax - xmin
    h = ymax - ymin
    xc = xmin + w/2
    yc = ymin + w/2
    
    return (xc, yc, w, h, xmin, ymin, xmax, ymax)
    
     
    

#check if a gun is inside a hand region and create an image if true
#hand_region: (xcentre, ycentre, width, height, xmin, ymin, xmax, ymax)
def checkAndExtract(hand_region, instances, img_name, img, inst_count, empty_count, test_count):    
    #check if hand region values extend past the image
    xmin = max(0, hand_region[4])
    ymin = max(0, hand_region[5])
    xmax = min(WIDTH, hand_region[6])
    ymax = min(HEIGHT, hand_region[7])
    hr_w = xmax - xmin
    hr_h = ymax - ymin
    
    #flag for if the box has no part of a gun in it
    isEmpty = True
    #iterate through gun instances
    for inst in instances:
        #normalised yolo format: (class xcentre ycentre width height)
        box = list(map(float, inst.split()))
        bx = box[1] * WIDTH
        by = box[2] * HEIGHT
        bw = box[3] * WIDTH
        bh = box[4] * HEIGHT
        
        #used to check if no part of the bounding box is in the hand region using intersection
        #must be of form (xmin, ymin, width, height)
        rect_a = (xmin, ymin, hr_w, hr_h)
        rect_b = (bx - bw/2, by - bh/2, bw, bh)
        intersection = getIntersection(rect_a, rect_b)
        #getIntersection returns (0,0,0,0) if no intersection
        if intersection[2] != 0:
            isEmpty = False
        
        #check if box lies inside the hand region
        if (bx - bw/2 > xmin) and (bx + bw/2 < xmax) and (by - bh/2 > ymin) and (by + bh/2 < ymax):
            
            if DATASET == 'test1' or DATASET == 'test2':
                new_img_name = img_name.replace('.jpg', '_' + METHOD_NAME + '_' + str(test_count) + '.jpg')
            else:
                new_img_name = img_name.replace('.jpg', '_' + METHOD_NAME + '_' + str(inst_count) + '.jpg')
            new_img_path = "../../../detect_" + METHOD_NAME + '/' + DATASET + "/images/" + new_img_name
            new_label_name = new_img_name.replace('.jpg', '.txt')
            new_label_path = "../../../detect_" + METHOD_NAME + '/' + DATASET + "/labels/" + new_label_name
            
            #calculate new xcentre, ycentre, width and height for bounding box
            x = (bx - xmin) / hr_w
            y = (by - ymin ) / hr_h
            w = bw / hr_w
            h = bh / hr_h
            new_box = "0 %f %f %f %f" % (x, y, w, h)
                
            try:
                #save the new image and label
                new_img = saveImage(new_img_path, new_label_path, img, (hr_w, hr_h, xmin, ymin, xmax, ymax), new_box)
                #show saved image and gun bounding box
                new_img = cv2.resize(new_img, (300, 300), interpolation = cv2.INTER_AREA)
                cv2.rectangle(new_img, (int((x - w/2)*300), int((y - h/2)*300)), (int((x + w/2)*300), int((y + h/2)*300)), (255,0,255), 2)
                cv2.imshow("Hand Region", new_img)
                test_count += 1
            except:
                print("Failed to save: " + new_img_name)
                
            #return if a gun is found and image is saved
            return inst_count, empty_count, test_count
        
        #increment count
        inst_count += 1
        
        
    #if out of for loop, then no gun bounding box was found inside the hand region
    #save if an empty image or test
    if isEmpty == True or DATASET == 'test1' or DATASET == 'test2':
        #save this as an no object image
        if DATASET == 'test1' or DATASET == 'test2':
            new_img_name = img_name.replace('.jpg', '_' + METHOD_NAME + '_' + str(test_count) + '.jpg')
            new_img_path = "../../../detect_" + METHOD_NAME + '/' + DATASET + "/images/" + new_img_name
            new_label_name = new_img_name.replace('.jpg', '.txt')
            new_label_path = "../../../detect_" + METHOD_NAME + '/' + DATASET + "/labels/" + new_label_name
        else:
            new_img_name = img_name.replace('.jpg', '_' + METHOD_NAME + '_empty_' + str(empty_count) + '.jpg')
            new_img_path = "../../../detect_" + METHOD_NAME + '/' + DATASET + "/images_empty/" + new_img_name
            new_label_name = new_img_name.replace('.jpg', '.txt')
            new_label_path = "../../../detect_" + METHOD_NAME + '/' + DATASET + "/labels_empty/" + new_label_name
            
        try:
            #save the new image and label
            new_img = saveImage(new_img_path, new_label_path, img, (hr_w, hr_h, xmin, ymin, xmax, ymax), "")
            empty_count += 1
            test_count += 1
        except Exception as e:
            print(e)
            print("Failed to save: " + new_img_name)
    
                
    return inst_count, empty_count, test_count

#save new image area dim from img where dim is a 6-tuple (w, h, xmin, ymin, xmax, ymax)
def saveImage(img_path, label_path, img, dim, box):
    #save the image
    new_img = img[int(dim[3]):int(dim[5]),int(dim[2]):int(dim[4])]
    #cv2.imwrite(img_path, new_img)
    
    #save the label
    #with open(label_path, 'w') as f:
        #write the handgun instance
        #f.write(box)
    
    #if test set, save the hand region dimensions so that the original bounding box can be recalculated
    #width, height, xmin, ymin
    if DATASET == 'test1' or DATASET == 'test2':
        with open(label_path.replace('labels', 'regions'), 'w') as f:
            region = "%f %f %f %f" % (int(dim[0]), int(dim[1]), dim[2], dim[3])
            f.write(region)
    
    return new_img


if __name__ == "__main__":
    try:
        # Import Openpose (Windows/Ubuntu/OSX)
        dir_path = os.path.dirname(os.path.realpath(__file__))
        try:
            # Windows Import
            if platform == "win32":
                # Change these variables to point to the correct folder (Release/x64 etc.)
                sys.path.append(dir_path + '/../python/openpose/Release');
                os.environ['PATH']  = os.environ['PATH'] + ';' + dir_path + '/../x64/Release;' +  dir_path + '/../bin;'
                import pyopenpose as op
            else:
                # Change these variables to point to the correct folder (Release/x64 etc.)
                sys.path.append('../../python');
                # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
                # sys.path.append('/usr/local/python')
                from openpose import pyopenpose as op
        except ImportError as e:
            print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
            raise e

        # Flags
        parser = argparse.ArgumentParser()
        parser.add_argument("--image_dir", default="../../../detect/" + DATASET + "/images/", help="Process an image. Read all standard formats (jpg, png, bmp, etc.).")
        parser.add_argument("--no_display", default=False, help="Enable to disable the visual display.")
        parser.add_argument("--fps_max", default="1")
        args = parser.parse_known_args()
        
        # Custom Params (refer to include/openpose/flags.hpp for more parameters)
        params = dict()
        params["model_folder"] = "../../models/"

        # Add others in path?
        for i in range(0, len(args[1])):
            curr_item = args[1][i]
            if i != len(args[1])-1: next_item = args[1][i+1]
            else: next_item = "1"
            if "--" in curr_item and "--" in next_item:
                key = curr_item.replace('-','')
                if key not in params:  params[key] = "1"
            elif "--" in curr_item and "--" not in next_item:
                key = curr_item.replace('-','')
                if key not in params: params[key] = next_item

        # Construct it from system arguments
        # op.init_argv(args[1])
        # oppython = op.OpenposePython()

        # Starting OpenPose
        opWrapper = op.WrapperPython()
        opWrapper.configure(params)
        opWrapper.start()
    
        imagePaths = op.get_images_on_directory(args[0].image_dir);

    
        for image_path in imagePaths:
            slash = len(args[0].image_dir)
            img_name = image_path[slash:]
            print("Processing: %s" % (img_name))
        
            # Process Image
            datum = op.Datum()
            imageToProcess = cv2.imread(image_path)
            img_resize = cv2.resize(imageToProcess, (WIDTH,HEIGHT), interpolation = cv2.INTER_AREA)
            img_display = img_resize.copy()
            datum.cvInputData = img_resize
            opWrapper.emplaceAndPop(op.VectorDatum([datum]))
        
            #if there are no keypoints then continue to next image
            try:
                if len(datum.poseKeypoints) == 0:
                    continue
            except:
                continue
        
            #get the label for the image
            label_path = image_path.replace('images', 'labels')
            label_path = label_path.replace('.jpg', '.txt')
            with open(label_path, 'r') as f:
                instances = f.readlines()
                #if there are no guns in the image then continue
            if len(instances) == 0:
                continue
            
            #reset counts (used for naming files)
            inst_count = 1
            empty_count = 1
            test_count = 1
            #iterate through the sets of body keypoints
            for keypoints in datum.poseKeypoints:
                #get the keypoint data for left and right arms
                #keypoints stored as [x, y, confidence score]
                rElbow = keypoints[3]
                rWrist = keypoints[4]
                lElbow = keypoints[6]
                lWrist = keypoints[7]
                #get neck and hip keypoints
                neck = keypoints[1]
                hip = keypoints[8]
                #calculate distance between neck and hip
                back_dist = math.sqrt( (hip[0] - neck[0])**2 + (hip[1] - neck[1])**2 )
            
                #check if right arm keypoints are present and somewhat confident
                if rElbow[2] > 0.45 and rWrist[2] > 0.45:
                
                    #get the hand region using specified method
                    #hand_region: (xcentre, ycentre, width, height, xmin, ymin, xmax, ymax)
                    if METHOD == 1:
                        rhand_region = getHandRegion1(rElbow, rWrist, back_dist)
                    elif METHOD == 2:
                        rhand_region = getHandRegion2(rElbow, rWrist, back_dist)
                    else:
                        continue
                
                    #check if a gun is inside the region and save accordingly 
                    inst_count, empty_count, test_count = checkAndExtract(rhand_region, instances, img_name, img_resize, inst_count, empty_count, test_count)
                
                    #display the hand region and centre point
                    cv2.rectangle(img_display, (int(rhand_region[4]), int(rhand_region[5])), (int(rhand_region[6]), int(rhand_region[7])), (255, 255, 0), 2)
                    cv2.circle(img_display, (int(rhand_region[0]), int(rhand_region[1])), 4, (255, 255, 0), -1)
                    #display wrist and elbow keypoints
                    cv2.circle(img_display, (int(rElbow[0]), int(rElbow[1])), 4, (0, 255, 255), -1)
                    cv2.circle(img_display, (int(rWrist[0]), int(rWrist[1])), 4, (0, 255, 255), -1)
                
                #repeat for left arm
                if lElbow[2] > 0.45 and lWrist[2] > 0.45:
                
                    #get the hand region using specified method
                    #hand_region: (xcentre, ycentre, width, height, xmin, ymin, xmax, ymax)
                    if METHOD == 1:
                        lhand_region = getHandRegion1(lElbow, lWrist, back_dist)
                    elif METHOD == 2:
                        lhand_region = getHandRegion2(lElbow, lWrist, back_dist)
                    else:
                        continue
                        
                    #check if a gun is inside the region and save accordingly
                    inst_count, empty_count, test_count = checkAndExtract(lhand_region, instances, img_name, img_resize, inst_count, empty_count, test_count)
                
                    #display the hand region and centre point
                    cv2.rectangle(img_display, (int(lhand_region[4]), int(lhand_region[5])), (int(lhand_region[6]), int(lhand_region[7])), (255, 255, 0), 2)
                    cv2.circle(img_display, (int(lhand_region[0]), int(lhand_region[1])), 3, (255, 255, 0), -1)
                    #display wrist and elbow keypoints
                    cv2.circle(img_display, (int(lElbow[0]), int(lElbow[1])), 4, (0, 255, 255), -1)
                    cv2.circle(img_display, (int(lWrist[0]), int(lWrist[1])), 4, (0, 255, 255), -1)


            if not args[0].no_display:
                # display the pose output data
                pose_resize = cv2.resize(datum.cvOutputData, (640, 480), interpolation = cv2.INTER_AREA)
                cv2.imshow("Pose Output Data", pose_resize)
                # Display hand regions
                img_display = cv2.resize(img_display, (640, 480), interpolation = cv2.INTER_AREA)
                cv2.imshow("Hand Region Extraction", img_display)
                key = cv2.waitKey(15)
                if key == 27: break
                time.sleep(1)
        
        #free the wrapper and gpu mem
        opWrapper = None
    
    except Exception as e:
        print(e)
        opWrapper = None
        sys.exit(-1)