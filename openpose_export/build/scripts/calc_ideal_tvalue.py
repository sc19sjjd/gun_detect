import sys
import cv2
import os
from sys import platform
import argparse
import math
import matplotlib.pyplot as plt
import numpy as np

WIDTH = 1280 
HEIGHT = 962

DATASET = "mgd_custom"

#returns closest point (p4) on line (p1,p2) to point p3
def closest(p1, p2, p3):
    x1, y1 = p1
    x2, y2 = p2
    x3, y3 = p3
    dx, dy = x2-x1, y2-y1
    det = dx*dx + dy*dy
    a = (dy*(y3-y1)+dx*(x3-x1))/det
    return (x1+a*dx, y1+a*dy)


def normaliseKeypoints(keypoints):
    neck = keypoints[1]
    neckx, necky, conf = neck
    
    hip = keypoints[8]
    hipx, hipy, conf = hip
    
    dist = math.sqrt((hipx-neckx)**2 + (hipy-necky)**2)
    
    normalised = []
    for keypoint in keypoints:
        ox = keypoint[0]
        oy = keypoint[1]
        if ox == 0 or oy == 0:
            new = [ox, oy]
        else:
            nx = (ox - neckx) / dist
            ny = (oy - necky) / dist
            new = [nx, ny]
        normalised.append(new)
        
    return normalised
    

try:
    # Import Openpose (Windows/Ubuntu/OSX)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    try:
        # Windows Import
        if platform == "win32":
            # Change these variables to point to the correct folder (Release/x64 etc.)
            sys.path.append(dir_path + '/../python/openpose/Release');
            os.environ['PATH']  = os.environ['PATH'] + ';' + dir_path + '/../x64/Release;' +  dir_path + '/../bin;'
            import pyopenpose as op
        else:
            # Change these variables to point to the correct folder (Release/x64 etc.)
            sys.path.append('../../python');
            # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
            # sys.path.append('/usr/local/python')
            from openpose import pyopenpose as op
    except ImportError as e:
        print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
        raise e

    # Flags
    parser = argparse.ArgumentParser()
    parser.add_argument("--image_dir", default="../../../datasets/" + DATASET + "/images/", help="Process an image. Read all standard formats (jpg, png, bmp, etc.).")
    parser.add_argument("--no_display", default=True, help="Enable to disable the visual display.")
    args = parser.parse_known_args()

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    params["model_folder"] = "../../models/"

# Add others in path?
    for i in range(0, len(args[1])):
        curr_item = args[1][i]
        if i != len(args[1])-1: next_item = args[1][i+1]
        else: next_item = "1"
        if "--" in curr_item and "--" in next_item:
            key = curr_item.replace('-','')
            if key not in params:  params[key] = "1"
        elif "--" in curr_item and "--" not in next_item:
            key = curr_item.replace('-','')
            if key not in params: params[key] = next_item

    # Construct it from system arguments
    # op.init_argv(args[1])
    # oppython = op.OpenposePython()

    # Starting OpenPose
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()
    
    imagePaths = op.get_images_on_directory(args[0].image_dir);
    
    x = np.array([])
    y = np.array([])
    
    for image_path in imagePaths:
        # Process Image
        datum = op.Datum()
        imageToProcess = cv2.imread(image_path)
        img_resize = cv2.resize(imageToProcess, (WIDTH,HEIGHT), interpolation = cv2.INTER_AREA)
        datum.cvInputData = img_resize
        opWrapper.emplaceAndPop(op.VectorDatum([datum]))
        
        print("Processing: %s" % (image_path[29:]))
        
        cv2.imshow("Pose Output Data", datum.cvOutputData)
        
        try:
            if len(datum.poseKeypoints) == 0:
                continue
        except:
            continue
        
        pdist = 10000
        dist = 1
        back_dist = 1
        p1 = (1,1)
        p2 = (1,1)
        p3 = (1,1)
        p4 = (1,1)
        t = 100
        nkeypoints = None
        
        #calculate closest point on line between elbow and wrist to centre of gun bbox
        for keypoints in datum.poseKeypoints:
            rHand = keypoints[4]
            rElbow = keypoints[3]
            neck = keypoints[1]
            hip = keypoints[8]
            #continue if confidence is not high enough
            if rHand[2] < 0.6 or rElbow[2] < 0.6:
                continue
    
            path = image_path.replace('images', 'labels_txt')
            path = path.replace('.jpg', '.txt')
            with open(path, 'r') as f:
                lines = f.readlines()
    
            if len(lines) == 0:
                continue
        
            box = lines[0].split()
        
            new_dist = math.sqrt( (rHand[0] - rElbow[0])**2 + (rHand[1] - rElbow[1])**2 )
        
            p1n = (float(rElbow[0]), float(rElbow[1]))
            p2n = (float(rHand[0]), float(rHand[1]))
            p3n = (float(box[1]) * WIDTH, float(box[2]) * HEIGHT)
            p4n = closest(p1n, p2n, p3n)
            tn = (p4n[0] - p1n[0]) / (p2n[0] - p1n[0])
        
            #new_dist = math.sqrt( (p2n[0] - p1n[0])**2 + (p2n[1] - p1n[1])**2 )
            new_pdist = math.sqrt( (p3n[0] - p4n[0])**2 + (p3n[1] - p4n[1])**2 )
            
            if new_pdist < pdist:
                pdist = new_pdist
                dist = new_dist
                p1 = p1n
                p2 = p2n
                p3 = p3n
                p4 = p4n
                t = tn
                back_dist = math.sqrt( (hip[0] - neck[0])**2 + (hip[1] - neck[1])**2 )
               # nkeypoints = normaliseKeypoints(keypoints)
        
        if pdist > dist * 1.5 or t > 2.5 or t < 0.5:
            continue
        
        x = np.append(x, dist)
        y = np.append(y, t)

        t = -0.001687 * dist + 1.576804
        x5 = p1[0] + t * (p2[0] - p1[0])
        y5 = p1[1] + t * (p2[1] - p1[1])
        
        if dist > back_dist:
            size = int(35 + dist / 1.3)
        else:
            size = int(25 + back_dist / 1.5)
        xmin = int(x5 - size / 2)
        xmax = int(x5 + size / 2)
        ymin = int(y5 - size / 2)
        ymax = int(y5 + size / 2)
        cv2.rectangle(img_resize, (xmin, ymin), (xmax, ymax), (255, 255, 0), 2)
        
        cv2.circle(img_resize, (int(p1[0]), int(p1[1])), 3, (0, 255, 255), -1)
        cv2.circle(img_resize, (int(p2[0]), int(p2[1])), 3, (0, 255, 255), -1)
        cv2.circle(img_resize, (int(p3[0]), int(p3[1])), 3, (255, 0, 150), -1)
        cv2.circle(img_resize, (int(p4[0]), int(p4[1])), 3, (0, 0, 255), -1)
        cv2.circle(img_resize, (int(x5), int(y5)), 3, (0, 255, 0), -1)
        cv2.imshow("t value estimation", img_resize)
        key = cv2.waitKey(15)
        if key == 27: break
        
    opWrapper = None
except Exception as e:
    print(e)
    opWrapper = None
    sys.exit(-1)

plt.axis([0, 120, 0, 3])
plt.ylabel('t value')
plt.xlabel('distance between wrist and forearm')
plt.title('t value against arm length for the ' + DATASET.upper())
plt.plot(x, y, 'ro', markersize=2)

try:
    a, b = np.polyfit(x, y, 1)
    line = np.linspace(0,120,500)
    plt.plot(line, a*line+b) 
except:
    pass

plt.show()
print("Best fit line (dist = x): t = %fx + %f" % (a, b))