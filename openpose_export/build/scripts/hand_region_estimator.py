import sys
import cv2
import os
from sys import platform
import argparse
import time
import math
import matplotlib.pyplot as plt
import numpy as np
#from shapely.geometry import LineString
#from shapely.geometry import Point

#returns closest point (p4) on line (p1,p2) to point p3
def closest(p1, p2, p3):
    x1, y1 = p1
    x2, y2 = p2
    x3, y3 = p3
    dx, dy = x2-x1, y2-y1
    det = dx*dx + dy*dy
    a = (dy*(y3-y1)+dx*(x3-x1))/det
    return (x1+a*dx, y1+a*dy)


def normaliseKeypoints(keypoints):
    neck = keypoints[1]
    neckx, necky, conf = neck
    
    hip = keypoints[8]
    hipx, hipy, conf = hip
    
    dist = math.sqrt((hipx-neckx)**2 + (hipy-necky)**2)
    
    normalised = []
    for keypoint in keypoints:
        ox = keypoint[0]
        oy = keypoint[1]
        if ox == 0 or oy == 0:
            new = [ox, oy]
        else:
            nx = (ox - neckx) / dist
            ny = (oy - necky) / dist
            new = [nx, ny]
        normalised.append(new)
        
    return normalised
    

try:
    # Import Openpose (Windows/Ubuntu/OSX)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    try:
        # Windows Import
        if platform == "win32":
            # Change these variables to point to the correct folder (Release/x64 etc.)
            sys.path.append(dir_path + '/../python/openpose/Release');
            os.environ['PATH']  = os.environ['PATH'] + ';' + dir_path + '/../x64/Release;' +  dir_path + '/../bin;'
            import pyopenpose as op
        else:
            # Change these variables to point to the correct folder (Release/x64 etc.)
            sys.path.append('../../python');
            # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
            # sys.path.append('/usr/local/python')
            from openpose import pyopenpose as op
    except ImportError as e:
        print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
        raise e

    # Flags
    parser = argparse.ArgumentParser()
    parser.add_argument("--image_dir", default="../../../datasets/mgd_custom/images/", help="Process an image. Read all standard formats (jpg, png, bmp, etc.).")
    parser.add_argument("--no_display", default=True, help="Enable to disable the visual display.")
    parser.add_argument("--fps_max", default="1")
    args = parser.parse_known_args()

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    params["model_folder"] = "../../models/"

# Add others in path?
    for i in range(0, len(args[1])):
        curr_item = args[1][i]
        if i != len(args[1])-1: next_item = args[1][i+1]
        else: next_item = "1"
        if "--" in curr_item and "--" in next_item:
            key = curr_item.replace('-','')
            if key not in params:  params[key] = "1"
        elif "--" in curr_item and "--" not in next_item:
            key = curr_item.replace('-','')
            if key not in params: params[key] = next_item

    # Construct it from system arguments
    # op.init_argv(args[1])
    # oppython = op.OpenposePython()

    # Starting OpenPose
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()
    
    imagePaths = op.get_images_on_directory(args[0].image_dir);
    
    x = np.array([])
    y = np.array([])
    
    for image_path in imagePaths:
        # Process Image
        datum = op.Datum()
        imageToProcess = cv2.imread(image_path)
        img_resize = cv2.resize(imageToProcess, (640,480), interpolation = cv2.INTER_AREA)
        datum.cvInputData = img_resize
        opWrapper.emplaceAndPop(op.VectorDatum([datum]))
        
        print("Processing: %s" % (image_path[29:]))
        
        cv2.imshow("Pose Output Data", datum.cvOutputData)
        
        try:
            if len(datum.poseKeypoints) == 0:
                continue
        except:
            continue
        p1 = (1,1)
        p2 = (1,1)
        p3 = (1,1)
        p4 = (1,1)
        p5 = (1,1)
        p6 = (1,1)
        
        for keypoints in datum.poseKeypoints:
            rHand = keypoints[4]
            rElbow = keypoints[3]
            neck = keypoints[1]
            hip = keypoints[8]
            #continue if confidence is not high enough
            if rHand[2] < 0.1 or rElbow[2] < 0.1:
                continue
            
            arm_dist = math.sqrt( (rHand[0] - rElbow[0])**2 + (rHand[1] - rElbow[1])**2 )
            back_dist = math.sqrt( (hip[0] - neck[0])**2 + (hip[1] - neck[1])**2 )
            if arm_dist > back_dist:
                t = 1.8 + arm_dist / 80
            else:
                t = 1.8 + back_dist / 100
            
            f = 3.5
            #t = 1.8 + max_dist / 90
            x = (1 - t) * rElbow[0] + t * rHand[0]
            y = (1 - t) * (rElbow[1]) + t * (rHand[1])
            p1 = (x, y)
            
            t = 0.88
            x = (1 - t) * rElbow[0] + t * rHand[0]
            y = (1 - t) * (rElbow[1]) + t * (rHand[1])
            p2 = (x, y)
        
        
            dx = (p1[0] - p2[0])
            dy = (p1[1] - p2[1])
            grad = float(dy / dx)
            grad_norm = -(1 / grad)
            
            #from top
            m = grad_norm
            c = p1[1] - grad_norm * p1[0]
            a = p1[0]
            b = p1[1]
            f = 6
            r = 20 + arm_dist / 2.5
            aprim = (1 + m**2)
            bprim = 2 * m * (c - b) - 2 * a
            cprim = a**2 + (c - b)**2 - r**2
            delta = bprim**2 - 4 * aprim * cprim
            x3 = (-bprim + math.sqrt(delta)) / (2 * aprim)
            y3 = m * x3 + c
            x4 = (-bprim - math.sqrt(delta)) / (2 * aprim)
            y4 = m * x4 + c
            
            p3 = (x3,y3)
            p4 = (x4,y4)
            
            
            #from bottom
            m = grad_norm
            c = p2[1] - grad_norm * p2[0]
            a = p2[0]
            b = p2[1]
            f = 9
            r = 15 + arm_dist / 4
            aprim = (1 + m**2)
            bprim = 2 * m * (c - b) - 2 * a
            cprim = a**2 + (c - b)**2 - r**2
            delta = bprim**2 - 4 * aprim * cprim
            x3 = (-bprim + math.sqrt(delta)) / (2 * aprim)
            y3 = m * x3 + c
            x4 = (-bprim - math.sqrt(delta)) / (2 * aprim)
            y4 = m * x4 + c
            
            p5 = (x3,y3)
            p6 = (x4,y4) 
            
            min_x = min(p1[0], p2[0])
            min_x = min(min_x, p3[0])
            min_x = min(min_x, p4[0])
            min_x = min(min_x, p5[0])
            min_x = int(min(min_x, p6[0]))
            max_x = max(p1[0], p2[0])
            max_x = max(max_x, p3[0])
            max_x = max(max_x, p4[0])
            max_x = max(max_x, p5[0])
            max_x = int(max(max_x, p6[0]))
            min_y = min(p1[1], p2[1])
            min_y = min(min_y, p3[1])
            min_y = min(min_y, p4[1])
            min_y = min(min_y, p5[1])
            min_y = int(min(min_y, p6[1]))
            max_y = max(p1[1], p2[1])
            max_y = max(max_y, p3[1])
            max_y = max(max_y, p4[1])
            max_y = max(max_y, p5[1])
            max_y = int(max(max_y, p6[1]))
            
            cv2.rectangle(img_resize, (min_x, min_y), (max_x, max_y), (0,0,255), 1)
        
            break

        
        cv2.circle(img_resize, (int(p1[0]), int(p1[1])), 3, (0, 255, 255), -1)
        cv2.circle(img_resize, (int(p2[0]), int(p2[1])), 3, (0, 255, 255), -1)
        cv2.circle(img_resize, (int(p3[0]), int(p3[1])), 3, (0, 255, 0), -1)
        cv2.circle(img_resize, (int(p4[0]), int(p4[1])), 3, (0, 255, 0), -1)
        cv2.circle(img_resize, (int(p5[0]), int(p5[1])), 3, (255, 0, 255), -1)
        cv2.circle(img_resize, (int(p6[0]), int(p6[1])), 3, (255, 0, 255), -1)
        cv2.imshow("box estimation", img_resize)
        key = cv2.waitKey(15)
        if key == 27: break
        
    opWrapper = None
except Exception as e:
    print(e)
    opWrapper = None
    sys.exit(-1)
    
opWrapper = None