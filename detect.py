#perform inference on an input image, directory of images or video
import sys
import cv2
import os
from sys import platform
import argparse
import math
import numpy as np
from os import walk
import re
import matplotlib.pyplot as plt
from yolov5 import detect
import time
import shutil
from contextlib import contextmanager
import torch
import torchvision


#input path, can be a directory of images, single image or video
INPUT_PATH = "C:/Users/Stuart/Documents/gun_detect/videos/gun_video.mp4"
#the trained model weights to use (use yolov5 detect.py for yolo only model for better performance)
MODEL = "C:/Users/Stuart/Documents/gun_detect/runs/train/hr_method1_final/weights/best.pt"

#0 for yolo only, 1 for method1, 2 for method2
METHOD = 2
assert METHOD == 0 or METHOD == 1 or METHOD ==2

#new fps output for video inputs
NEW_FPS =  30


#iou threshold for NMS
NMS_THRESH = 0.5
CONF_THRESH = 0.5

#silences console outputs when activated
@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:  
            yield
        finally:
            sys.stdout = old_stdout
            

#union of 2 rectangles a, b
#a and b in the form (xmin, ymin, width, height)
def getUnion(a, b):
    area1 = a[2] * b[3]
    area2 = b[2] * b[3]
    intersection  = getIntersection(a, b)
    #union = area1 + area2 - getIntersection(a, b)
    union = area1 + area2 - intersection
    
    return union

#intersection of rectangles a, b
#a and b in the form (xmin, ymin, width, height)
def getIntersection(a, b):
    x = max(a[0], b[0])
    y = max(a[1], b[1])
    w = min(a[0] + a[2], b[0] + b[2]) - x
    h = min(a[1] + a[3], b[1] + b[3]) - y
    intersection = w * h
    
    if w < 0 or h < 0:
        return 0
    else:
        return intersection

#get the IoU of 2 rectangles a, b
#a and b in the form (xmin, ymin, width, height)
def getIoU(a, b):
    intersection = getIntersection(a, b)
    union = getUnion(a, b)
    iou = intersection / union
    
    return iou

#perform non max suppression on a list of instances of bounding boxes
def NMS(instances, threshold):
    #sort instances in order of confidence score
    in_order = []
    is_suppressed = []
    max_val = 0
    max_i = 0
    for i in range(len(instances)):
        for j in range(len(instances)):
            if float(instances[j].split()[5]) > max_val:
                max_val = float(instances[j].split()[5])
                max_i = j
                
        
        is_suppressed.append(False)
        in_order.append(instances[max_i])
        instances.pop(max_i)
        max_val = 0
    instances = in_order

    #iterate through all instances ordered by highest conf score
    for i in range(len(instances)):
        #check if the box has already been suppressed
        if is_suppressed[i]:
            continue
        
        bbox1 = list(map(float, instances[i].split()))
        #now perform non max suppression on the highest confidence and all other bndboxes
        for j in range(len(instances)):
            if i == j:
                continue
            bbox2 = list(map(float, instances[j].split()))
            #rectangles must be of form (xmin, ymin, width, height)
            a = (float(bbox1[1]) - float(bbox1[3])/2, float(bbox1[2]) - float(bbox1[4])/2, float(bbox1[3]), float(bbox1[4])) 
            b = (float(bbox2[1]) - float(bbox2[3])/2, float(bbox2[2]) - float(bbox2[4])/2, float(bbox2[3]), float(bbox2[4])) 
            iou = getIoU(a, b)
            #if iou is above threshold, then suppress the lower confidence bbox
            if iou > threshold:
                is_suppressed[j] = True
     
    new_instances = []
    #now keep all unsuppressed boxes
    for i in range(len(instances)):
        if not is_suppressed[i]:
            new_instances.append(instances[i])
    
    return new_instances
            
#rescales the hand region bounding boxes for the full image size
def rescale(instances, img_dims):
    new_instances = []
    #iterate through instances
    for instance in instances:
        bbox = list(map(float, instance.split()))
        #calculate original xcentre, ycentre, width and height for bounding box
        #x = ((bbox[1] * img_dims[0]) + hr_xmin) / IMG_WIDTH
        #y = ((bbox[2] * img_dims[1]) + hr_ymin) / IMG_HEIGHT
        w = (bbox[3] * img_dims[0])
        h = (bbox[4] * img_dims[1])
        #original_bbox = "%f %f %f %f %f %f" % (bbox[0], x, y, w, h, bbox[5])
        #add rescaled bounding box instance
       # new_instances.append(original_bbox) 
        
    return new_instances

#method 1 for hand region estimation, elbow and wrist are keypoints
def getHandRegion1(elbow, wrist, back_dist):
    #get distance between elbow and wrist
    arm_dist = math.sqrt( (wrist[0] - elbow[0])**2 + (wrist[1] - elbow[1])**2 )

    #calculate centre of hand region using parametric form of line
    t = -0.001687 * arm_dist + 1.576804
    xc = (1 - t) * elbow[0] + t * wrist[0]
    yc = (1 - t) * elbow[1] + t * wrist[1]
        
    #use back or arm length to determine the size of the region
    #size = 25 + back_dist / 1.5
    if arm_dist > back_dist:
        size = 35 + arm_dist / 1.2
    else: 
        size = 25 + back_dist / 1.2
    w = size
    h = size
    
    xmin = float(xc - size / 2)
    xmax = float(xc + size / 2)
    ymin = float(yc - size / 2)
    ymax = float(yc + size / 2)
    
    return (xc, yc, w, h, xmin, ymin, xmax, ymax)

#get the points of intersection between a line y = mx + c that goes 
#through the centre of a circle r^2 = (x - a)^2 + (y - b)^2
def circleIntersection(m, c, a, b, r):
    aprim = (1 + m**2)
    bprim = 2 * m * (c - b) - 2 * a
    cprim = a**2 + (c - b)**2 - r**2
    delta = bprim**2 - 4 * aprim * cprim
    
    x1 = (-bprim + math.sqrt(delta)) / (2 * aprim)
    y1 = m * x1 + c
    p1 = (x1, y1)
    
    x2 = (-bprim - math.sqrt(delta)) / (2 * aprim)
    y2 = m * x2 + c
    p2 = (x2, y2)
    
    return p1, p2

#returns the max value from a list
def getMax(numbers):
    m = numbers[0]
    for num in numbers:
        m = max(m, num)
        
    return m

#returns the minimum number from a list
def getMin(numbers):
    m = numbers[0]
    for num in numbers:
        m = min(m, num)
        
    return m

#method 2 for hand region estimation
def getHandRegion2(elbow, wrist, back_dist):
    #get distance between elbow and wrist
    arm_dist = math.sqrt( (wrist[0] - elbow[0])**2 + (wrist[1] - elbow[1])**2 )
    
    #calculate t for the point (p1) that extends past the gun
    t = 2.5
    #extend the line from elbow to wrist
    x = (1 - t) * elbow[0] + t * wrist[0]
    y = (1 - t) * elbow[1] + t * wrist[1]
    p1 = (x, y)
    
    #calculate the point (p2) that is slightly behind the wrist, between the elbow
    t = 0.88
    x = (1 - t) * elbow[0] + t * wrist[0]
    y = (1 - t) * elbow[1] + t * wrist[1]
    p2 = (x, y)
    
    #now calculate the gradient of the normal for the line from p1 and p2
    dx = (p1[0] - p2[0])
    dy = (p1[1] - p2[1])
    grad = dy / dx
    grad_norm = -(1 / grad)
    
    #find points p3 and p4 that are distance r away from point p1 and lie on the normal
    #where r is the radius of a circle with centre p1 that intersects the normal line at p3 and p4
    r = 18 + arm_dist / 1.1
    #find c for the normal line y = grad_norm*x + c that goes through p1, p3 and p4
    c = p1[1] - grad_norm * p1[0]
    #get the points of intersection
    p3, p4 = circleIntersection(grad_norm, c, p1[0], p1[1], r)
    
    #find points p5 and p6 that are distance r away from point p2 and lie on the normal
    #where r is the radius of a circle with centre p2 that intersects the normal line at p4 and p5
    r = 12 + arm_dist / 1.6
    #find c for the normal line y = grad_norm*x + c that goes through p2, p5 and p6
    c = p2[1] - grad_norm * p2[0]
    #get the points of intersection
    p5, p6 = circleIntersection(grad_norm, c, p2[0], p2[1], r)
    
    #now get the hand region values
    xmin = getMin([p1[0], p2[0], p3[0], p4[0], p5[0], p6[0]])
    ymin = getMin([p1[1], p2[1], p3[1], p4[1], p5[1], p6[1]])
    xmax = getMax([p1[0], p2[0], p3[0], p4[0], p5[0], p6[0]])
    ymax = getMax([p1[1], p2[1], p3[1], p4[1], p5[1], p6[1]])
    w = xmax - xmin
    h = ymax - ymin
    xc = xmin + w/2
    yc = ymin + w/2
    
    return (xc, yc, w, h, xmin, ymin, xmax, ymax)


def getInput():
    #first check what type the input is (0 = single image, 1 = image directory, 2 = video)
    if INPUT_PATH.lower().endswith(('.png', '.jpeg', 'jpg')):
        input_type = 0
    elif os.path.isdir(INPUT_PATH):
        input_type = 1
    elif INPUT_PATH.lower().endswith(('mp4', '.wav')):
        input_type = 2
    else:
        print("Input type not supported. Terminating...")
        exit(1)
    
    #holds file names of all images to be processed
    fnames = []
    #only 1 image to process
    if input_type == 0:
        path_split = INPUT_PATH.split('/')
        fnames.append(path_split[-1])
        image_path = INPUT_PATH.replace(fnames[0], '')
        #directory of images input
    elif input_type == 1:
        #get all img file names from full images test set path
        for (dirpath, dirnames, filenames) in walk(INPUT_PATH):
            fnames.extend(filenames)
            break
        image_path = INPUT_PATH
    #video input
    elif input_type == 2:
        try:
            video = cv2.VideoCapture(INPUT_PATH)
        except Exception as e:
            print("Failure to read video input\n" + str(e))
            exit(1)
    
        try:
            # creating a folder named data
            if not os.path.exists('../gun_detect/temp_imgs'):
                os.makedirs('temp_imgs')
            #else:
            #    print("temp_data directory already exists, please delete or rename this directory")
            #    sys.exit(-1)
        # if not created then raise error
        except OSError:
            print('Error: Creating directory of temp data')
    
    
        print("Splitting video...")
        #get the frames from the video
        frame_count = 0
        orig_fps = int(video.get(cv2.CAP_PROP_FPS))
        current_frame = 0
        while (True):
            # reading from frame
            ret, frame = video.read()

            if ret:
                # if video is still left continue creating images
                # writing the extracted images
                if current_frame % (int(orig_fps / NEW_FPS)) == 0:
                    fname= "frame" + str(frame_count) + '.jpg'
                    cv2.imwrite("temp_imgs/" + fname, frame)
                    fnames.append(fname)

                    frame_count += 1
            else:
                break
        
            current_frame += 1    
        
        # Release all space once done
        video.release()
        cv2.destroyAllWindows()
        image_path = 'C:/Users/Stuart/Documents/gun_detect/temp_imgs/'
    else:
        print("Input type not supported")
        exit(1)
        
    return fnames, image_path, input_type
    
def detectRegions(regions, img, img_count):
    img_path = 'temp_data/' + str(img_count) 
    
    if not os.path.exists(img_path):
        os.makedirs(img_path)
    
    dim = img.shape
    
    #save the regions in a temp directory
    rcount = 0
    for region in regions:
        #check if hand region values extend past the image
        xmin = int(max(0, region[4]))
        ymin = int(max(0, region[5]))
        xmax = int(min(dim[1], region[6]))
        ymax = int(min(dim[0], region[7]))
        hr_w = int(xmax - xmin)
        hr_h = int(ymax - ymin)
        
        img_region = img[ymin:ymax,xmin:xmax]
        cv2.imwrite('../gun_detect/' + img_path + '/' + str(rcount) + '.jpg', img_region)
        rcount += 1
    
    #perform the yolo detection
    with suppress_stdout():
        os.system(("python %s --weights %s --source %s --conf-thres %s --iou-thres %s --save-txt --save-conf --project %s --name %s --nosave" %
                   ('C:/Users/Stuart/Documents/gun_detect/yolov5/detect.py',
                    MODEL,
                    'C:/Users/Stuart/Documents/gun_detect/' + img_path,
                    str(CONF_THRESH),
                    str(NMS_THRESH),
                    'C:/Users/Stuart/Documents/gun_detect/' + img_path,
                    'exp')))
    
    #display the bounding box predictions
    count = 0
    instances = []
    for region in regions:
        #check if hand region values extend past the image
        xmin = int(max(0, region[4]))
        ymin = int(max(0, region[5]))
        xmax = int(min(dim[1], region[6]))
        ymax = int(min(dim[0], region[7]))
        hr_w = int(xmax - xmin)
        hr_h = int(ymax - ymin)
        
        try:
            with open(img_path + "/exp/labels/" + str(count) + ".txt", 'r') as f:
                temp_instances = f.readlines()
                for instance in temp_instances:
                    #rescale the instance
                    box = list(map(float, instance.split()))
                    bx = int(box[1] * hr_w + xmin)
                    by = int(box[2] * hr_h + ymin)
                    bw = int(box[3] * hr_w)
                    bh = int(box[4] * hr_h)
                    
                    instances.append("%f %f %f %f %f %f" % (box[0], bx, by, bw, bh, box[5]))
        except:
            continue
    
        count += 1
    
    #non max suppression
    instances = NMS(instances, NMS_THRESH)
    
    for i in range(len(instances)):
        box = list(map(float, instances[i].split()))
        bx = int(box[1])
        by = int(box[2])
        bw = int(box[3])
        bh = int(box[4])
        conf = box[5]
            
        cv2.rectangle(img, (int(bx-bw/2), int(by-bh/2)), (int(bx+bw/2), int(by+bh/2)), (255,0,255), 3)
        cv2.putText(img, "{:.2f}".format(conf), (int(bx-bw/2), int(by-bh/2 - 5)), cv2.FONT_HERSHEY_DUPLEX, 1.5, (255,0,255), 3, 1)
        

    shutil.rmtree('../gun_detect/temp_data/' + str(img_count))
    
    
    return
            
def saveImages(img_list, pose_list):
    count = 0
    for i in range(len(img_list)):
        img_name = "inference_output" + str(count) + ".jpg"
        pose_name = "inference_pose_output" + str(count) + ".jpg"
        cv2.imwrite("inference/" + img_name, img_list[i])
        cv2.imwrite("inference/" + pose_name, pose_list[i])
        count += 1

def rebuildVideo(img_list, pose_list):
    h, w, l = img_list[0].shape
    
    out_pose = cv2.VideoWriter('inference/inference_pose_output.mp4', cv2.VideoWriter_fourcc(*'H246'), NEW_FPS, (w, h), 1)
    out = cv2.VideoWriter('inference/inference_output.mp4', cv2.VideoWriter_fourcc(*'H246'), NEW_FPS, (w, h), 1)
    
    for i in range(len(img_list)):
        out.write(img_list[i])
        out_pose.write(pose_list[i])
    
    out.release()
    

if __name__ == "__main__":
    t_start = time.time()
    
    fnames, image_path, input_type = getInput()
    
    # Import Openpose (Windows/Ubuntu/OSX)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    try:
        # Windows Import
        if platform == "win32":
            # Change these variables to point to the correct folder (Release/x64 etc.)
            sys.path.append(dir_path + '/openpose_export/build/python/openpose/Release');
            os.environ['PATH']  = os.environ['PATH'] + ';' + dir_path + '/openpose_export/build/x64/Release;' +  dir_path + '/openpose_export/build/bin;'
            import pyopenpose as op
        else:
            # Change these variables to point to the correct folder (Release/x64 etc.)
            sys.path.append('../../python');
            # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
            # sys.path.append('/usr/local/python')
            from openpose import pyopenpose as op
    except ImportError as e:
        print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
        raise e

    # Flags
    parser = argparse.ArgumentParser()
    #parser.add_argument("--image_dir", default="../../../detect/" + DATASET + "/images/", help="Process an image. Read all standard formats (jpg, png, bmp, etc.).")
    parser.add_argument("--no_display", default=False, help="Enable to disable the visual display.")
    parser.add_argument("--fps_max", default="1")
    args = parser.parse_known_args()
        
    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    params["model_folder"] = "openpose_export/models/"

    # Add others in path?
    for i in range(0, len(args[1])):
        curr_item = args[1][i]
        if i != len(args[1])-1: next_item = args[1][i+1]
        else: next_item = "1"
        if "--" in curr_item and "--" in next_item:
            key = curr_item.replace('-','')
            if key not in params:  params[key] = "1"
        elif "--" in curr_item and "--" not in next_item:
            key = curr_item.replace('-','')
            if key not in params: params[key] = next_item

    # Construct it from system arguments
    # op.init_argv(args[1])
    # oppython = op.OpenposePython()

    # Starting OpenPose
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()
    
    img_list = []
    pose_list = []
    img_count = 0
    for fname in fnames:
        # Process Image
        datum = op.Datum()
        image = cv2.imread(image_path + fname)
        #img_display = cv2.resize(imageToProcess, (640, 480), interpolation = cv2.INTER_AREA)
        datum.cvInputData = image
        opWrapper.emplaceAndPop(op.VectorDatum([datum]))
        
        #save the image with pose keypoints
        pose_list.append(datum.cvOutputData)
        
        #if there are no keypoints then continue to next image
        try:
            if len(datum.poseKeypoints) == 0:
                img_list.append(image)
                img_count += 1
                continue
        except:
            continue
        
        regions = []
        #iterate through the sets of body keypoints
        for keypoints in datum.poseKeypoints:
            #get the keypoint data for left and right arms
            #keypoints stored as [x, y, confidence score]
            rElbow = keypoints[3]
            rWrist = keypoints[4]
            lElbow = keypoints[6]
            lWrist = keypoints[7]
            #get neck and hip keypoints
            neck = keypoints[1]
            hip = keypoints[8]
            #calculate distance between neck and hip
            back_dist = math.sqrt( (hip[0] - neck[0])**2 + (hip[1] - neck[1])**2 )
            
            #check if right arm keypoints are present and somewhat confident
            if rElbow[2] > 0.45 and rWrist[2] > 0.45:
                #print("right")
                #get the hand region using specified method
                #hand_region: (xcentre, ycentre, width, height, xmin, ymin, xmax, ymax)
                if METHOD == 1:
                    rhand_region = getHandRegion1(rElbow, rWrist, back_dist)
                elif METHOD == 2:
                    rhand_region = getHandRegion2(rElbow, rWrist, back_dist)

                regions.append(rhand_region)

            #check if right arm keypoints are present and somewhat confident
            if lElbow[2] > 0.45 and lWrist[2] > 0.45:
            
                #get the hand region using specified method
                #hand_region: (xcentre, ycentre, width, height, xmin, ymin, xmax, ymax)
                if METHOD == 1:
                    lhand_region = getHandRegion1(lElbow, lWrist, back_dist)
                elif METHOD == 2:
                    lhand_region = getHandRegion2(lElbow, lWrist, back_dist)
                #print("left")
                regions.append(lhand_region)

        if len(regions) > 0:
            #time.sleep(2)
            detectRegions(regions, image, img_count)
            img_count += 1

        cv2.imshow("Inference", image)
        key = cv2.waitKey(15)
        if key == 27: break
    
        img_list.append(image)    
    
    #remove the temporary directory(s)
    shutil.rmtree('../gun_detect/temp_data')
    if (input_type == 2):
        rebuildVideo(img_list, pose_list)
        shutil.rmtree('../gun_detect/temp_imgs')
    else:
        saveImages(img_list, pose_list)
    opWrapper = None
    
    t_end = time.time()
    print("\n\n\nExecution time in seconds: %i \n\n\n" % (t_end - t_start))



















































