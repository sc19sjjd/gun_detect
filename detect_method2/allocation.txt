Guns:

gmd: 
  train - 1079
  valid - 250
  test  - 0
  total - 1329

mgd_custom:
  train - 1543
  valid - 376
  test  - 0
  total - 1919

mgd_varying:
  train - 561
  valid - 147
  test  - 0
  total - 708

youtube:
  train	- 327
  valid - 88
  test  - 0
  total - 415

TOTAL:
  train - 3510
  valid - 861
  total - 4371

ADDED 10% of empty images to val and train

TOTALS:

gmd: 
  train - 1185
  valid - 277
  test  - 0
  total - 1469

mgd_custom:
  train - 1744
  valid - 427
  test  - 0
  total - 2170

mgd_custom_test:
  train - 0
  valid - 0
  test1 - 831
  test2 - 0
  total - 831

mgd_varying:
  train - 568
  valid - 149
  test  - 0
  total - 718

mad:
  train - 0
  valid - 0
  test1 - 0
  test2 - 1895
  total - 1895

youtube:
  train	- 363
  valid - 95
  test  - 0
  total - 437

TOTAL:
  train - 3860
  valid - 948
  test1 - 831
  test2 - 1895
  total - 7534